function [ c_f1,jacc_f1, somme_hess_cf1 ] = cf1()
%cette fonction sans paramètre renvoie un handler sur la contrainte 
%d'égalité de la fonction f1

c_f1 = @ff;
jacc_f1 = @ff2;
somme_hess_cf1 = @ff3;

function [y] = ff(x)
    y = x(1) + x(3) - 1;
end
function [y] = ff2(x)
    y = [1 0 1];
end
function [y] = ff3(x)
    y = zeros(3,3);
end

end

