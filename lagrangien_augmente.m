function [x_min,lambda,mu,flag,nb_eval_f,nb_iter,f_xmin, gradf_xmin]=lagrangien_augmente(f, grad_f, hess_f, c, jac_c, somme_hess_ci,  x0 , lambda0, tau, alpha, beta, eps2, eps3, algo)
%% Algorithme du lagrangien augmente
% Résout un problème de minimisation avec contraintes d'égalité en
% % résolvant une suite de sous-problèmes sans contraintel
% Paramètres : 
% f, grad_f, hess_f : les fonctions f, gradient et hessienne de f
% c : fonction qui représente les contraintes d'égalité
% jac_c : fonction qui représente jacobienne de c
% somme_hess_ci : fonction qui à un vecteur x associe la somme des
% hessiennes des contraintes calculée en x (nécessaire pour le calcul de la
% hessienne du lagrangien augmenté
% x0 : point de départ de l'algorithme
% lambda0 : multiplicateur de lagrange associé à x0
% tau : réel qui contrôle l'augmentation du paramètre de pénalité mu
% alpha, beta : réels qui contrôlent l'évolution des eta_k
% eps2 : tolérance sur la CN1 (norme de (gradient de f(x) + lambda'*c(x))
% eps3 : tolérance sur la norme des contraintes d'égalité
% algo = 1 pour résolution du sous-problème avec newton, 2 pour RC Cauchy, 
% 3 pour RC More-Sorensen
% Retour : 
% x_min : minimum de la fonction
% lambda : multiplicateur de lagrange associé au minimum
% mu : valeur finale du paramètre de pénalité
% flag : indique la condition de sortie : 1 pour dépassement de maxIter, 2
% pour stagnation des itérés, 3 pour CN1 (norme du gradient du lagrangien),
% 4 pour dépassement de max_nb_eval_f
% nb_eval_f : nombre d'évaluations de la fonction f
% nb_iter : nombre d'itérations effectuées 
% f_xmin, gradf_xmin : valeurs de f et du gradient de f à la sortie de
% l'algorithme

%%  Initialisation des constantes 
maxIter = 50;
eta0chap = 0.1258925;
mu0 = 10;
eps0 = 1/mu0;
eta0 = eta0chap/(mu0^alpha);
gardefou = 1; % pour casser les facteurs d'échelle liés au calcul du gradient
max_nb_eval_f = 100;
grad_fx0 = grad_f(x0);


%% Initialisation des variables
x = x0;
eta = eta0;
lambda = lambda0;
eps = eps0;
mu = mu0;
k = 0;
nb_eval_f = 0;
normeX = norm(x);
diff_normeX = eps2*(normeX+gardefou) + 1; % pour que cette condition ne soit pas fausse dès la 1re itération
normeCN1 = eps2 + 1; %idem
normeC = eps3 + 1; %idem

while((k < maxIter) && ((normeCN1 > eps2) || (normeC > eps3)) && (nb_eval_f < max_nb_eval_f))
    %% minimiser L_A
    % calculer la fonction L_A et sa fonction gradient
    % calculer comb_h_c la somme qui apparaît dans la hessienne du
    % lagrangien augmenté
    
    [comb_h_c] = combhc(lambda, mu, c, somme_hess_ci);
    [L, grad_L, hess_L] = L_A(f, grad_f, hess_f, c, jac_c, comb_h_c, lambda, mu);
    
    x_prec = x;
    %% Résoudre le sous-problème sans contraintes avec la méthode choisie
    if (algo == 1)
        [x,flag,nb_eval_phi,nb_iter,phi_xmin]=newton(grad_L, hess_L,x, eps/(norm(grad_fx0)+gardefou) ,eps2);
        %on finit lorsque la norme du gradient de L_A <= eps_k
    elseif (algo == 2)
        [x,flag,nb_eval_f,nb_iter,f_xmin, gradf_xmin] = regions_confiance(L, grad_L, hess_L,x, eps/(norm(grad_fx0)+gardefou), eps2, 1e8, 1, 0.5, 2, 0.25, 0.75);
    elseif (algo == 3)
        [x,flag,nb_eval_f,nb_iter,f_xmin, gradf_xmin] = regions_confiance_MS(L, grad_L, hess_L,x, eps/(norm(grad_fx0)+gardefou), eps2, 1e8, 1, 0.5, 2, 0.25, 0.75);
    else
        disp('Veuillez rentrer algo = 1,2 ou 3 en paramètre');
        return;
    end
    
    %On calcule les quantités utilisées pour le prochain test de
    %convergence dans le while
    diff_normeX = norm(x_prec - x);
    normeCN1 = norm(grad_f(x)+lambda'*(jac_c(x)'));
    normeC = norm(c(x));
    %% Si on a convergé, alors on sort de l'algorithme
    if ((normeCN1 <= eps2) && (normeC <= eps3 ))
        break;
    end
    
    %% Sinon, mettre à jour les multiplicateurs et/ou le paramètre de pénalité
    if (normeC <= eta)
        lambda = lambda + mu*c(x);
        % mu ne change pas
        eps = eps/mu;
        eta = eta/mu^beta;
        k = k + 1;
    else
        % lambda ne change pas
        mu = tau*mu;
        eps = eps0/mu;
        eta = eta0chap/mu^alpha;
        k = k + 1;
    end
    
end

x_min = x;

%% Positionnement du flag de sortie
if (k >= maxIter)
    flag = 1;
elseif (diff_normeX <= eps2*(normeX+eps0))
    flag = 2;
elseif (norm(grad_f(x)+lambda'*(jac_c(x)')) <= eps3)
    flag = 3;
elseif (nb_eval_f >= max_nb_eval_f)
    flag = 4;
else
    flag = -1;
end
%% Affectation des sorties de l'algorithme
nb_iter = k;
f_xmin = f(x_min);
gradf_xmin = grad_f(x_min);

end
