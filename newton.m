function [x_min,flag,nb_eval_phi,nb_iter,phi_xmin]=newton(phi, jac_phi,x0, eps1, eps2)
%% Algorithme de Newton local
% Calcule le minimum d'une fonction en l'approximant par un modèle
% quadratique
% Paramètres : 
% phi : vecteur gradient de la quadratique
% jac_phi : matrice jacobienne de la quadratique
% x0 : point de départ de l'algorithme
% eps1 , eps2 : tolérances respectives sur la norme du gradient et la
% stagnation des itérés
% Retour : 
% x_min : minimum de la quadratique
% flag : indique la condition de sortie : 1 pour dépassement de maxIter, 2
% pour stagnation des itérés, 3 pour CN1 (norme du gradient),
% 4 pour dépassement de max_nb_eval_f
% nb_eval_phi : nombre d'évaluations de la fonction phi
% nb_iter : nombre d'itérations effectuées
% phi_xmin : valeur de la fonction en le zéro calculé

%% Initialisation des constantes et des variables
maxIter = 200;
x=x0;
k=0;
normeX = norm(x);
phiX = phi(x);
phiX0 = phi(x0);
norme_phiX0 = norm(phiX0);
eps0 = 1; %garde fou pour conditions d'arret
diff_normeX=eps2*(normeX+eps0) + 1;
nb_eval_phi = 0;
max_nb_eval_phi = 100;

while(((k < maxIter) && (diff_normeX > eps2*(normeX+eps0)) && (norm(phiX) > eps1*(norme_phiX0+eps0)) && (nb_eval_phi < max_nb_eval_phi) ))
    jacX = jac_phi(x);
    phiX = phi(x);
    nb_eval_phi = nb_eval_phi + 1;
    % Calcul de la direction de descente
    d = -jacX\phiX;
    x = x + d;
    normeX = norm(x);
    diff_normeX = norm(d); %norme de x_k+1 - x_k
    phiX = phi(x);
    k = k+1;
end
x_min=x;
    %% Positionnement des flags de sortie et des sorties de l'algorithme
if (k >= maxIter)
    flag = 1;
elseif (diff_normeX <= eps2*(normeX+eps0))
    flag = 2;
elseif (norm(phiX) <= eps1*(norme_phiX0+eps0))
    flag = 3;
elseif (nb_eval_phi >= max_nb_eval_phi)
    flag = 4;
else
    flag = -1;
end
nb_iter = k;
phi_xmin = phi(x);

end
    