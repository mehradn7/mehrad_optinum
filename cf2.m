function [ c_f2,jacc_f2, somme_hess_cf2 ] = cf2()
%cette fonction sans paramètre renvoie un handler sur la contrainte 
%d'égalité de la fonction f2

c_f2 = @ff;
jacc_f2 = @ff2;
somme_hess_cf2 = @ff3;

function [y] = ff(x)
    y = x(1)^2 + x(2)^2 - 1.5;
end
function [y] = ff2(x)
    y = [2*x(1) 2*x(2)];
end
function [y] = ff3(x)
    y = [2 0; 0 2];
end

end

