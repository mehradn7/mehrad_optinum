function [ phi ] = S_tilde(Q, g, D)
%cette fonction renvoie la fonction s tilde 
%qui intervient dans l'algorithme de moré sorensen

phi = @phiAUX;

function [y] = phiAUX(lambda)
C = -(Q'*g); %vecteur colonne
I = find(abs(C)>1e-8);
B = D + lambda;
y = Q(:,I)*(C(I) ./ B(I));
end


end