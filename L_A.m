function [ phi, phip, phipp ] = L_A(f, grad_f, hess_f, c, jac_c, comb_h_c, lambda, mu)
%cette fonction renvoie un handler sur la fonction lagrangien augmenté, 
% son gradient et sa hessienne

phi = @phiAUX;
phip = @phipAUX;
phipp = @phippAUX;


function [y] = phiAUX(x)
    y = f(x) + (lambda')*c(x) + 0.5*mu*norm(c(x))^2;
end

function [y] = phipAUX(x)
    y = grad_f(x) + jac_c(x)'*(lambda + mu*c(x));
end

function [y] = phippAUX(x)
    y = hess_f(x) + mu*(jac_c(x)')*jac_c(x) + comb_h_c(x);
end


end