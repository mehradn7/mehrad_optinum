function [x_min,flag,nb_eval_f,nb_iter,f_xmin, gradf_xmin]=regions_confiance(f, grad_f, hess_f,x0, eps1, eps2, delta_max, delta0, gamma1, gamma2, eta1, eta2)
%% Algorithme des régions de confiance avec pas de Cauchy
% Résout un problème de minimisation sans contraintes à l'aide du pas de
% Cauchy (direction du gradient)
% Paramètres : 
% f, grad_f, hess_f : les fonctions f, gradient et hessienne de f
% x0 : point de départ de l'algorithme
% eps1 : tolérance sur la CN1 (norme de (gradient de f(x) )
% eps2 : tolérance sur la stagnation des itérés
% delta_max : largeur maximale des régions de confiance
% delta0 : largeur de la région de confiance initiale
% gamma1 : facteur de réduction de la région de confiance en cas d'échec
% de l'itération
% gamma2 : facteur d'augmentation de la région de confiance en cas de succès
% de l'itération
% eta1, eta2 : réel compris entre 0 et 1 ; si rho le rapport de
% satisfaction associé à l'itération est inférieur à eta1, alors
% l'itération est un échec ; si rho est compris entre eta1 et eta2 :
% l'itération est réussie : si rho est supérieur à eta2 : l'itération est
% très réussie
% Retour : 
% x_min : minimum de la fonction
% flag : indique la condition de sortie : 1 pour dépassement de maxIter, 2
% pour stagnation des itérés, 3 pour CN1 (norme du gradient du lagrangien),
% 4 pour dépassement de max_nb_eval_f
% nb_eval_f : nombre d'évaluations de la fonction f
% nb_iter : nombre d'itérations effectuées 
% f_xmin, gradf_xmin : valeurs de f et du gradient de f à la sortie de
% l'algorithme

%% Initialisation des constantes et des variables
maxIter = 200;
x=x0;
k=0;
normeX = norm(x);
grad_fx0 = grad_f(x0);
eps0 = 1; %garde fou pour conditions d'arret
norm_gradfx = norm(grad_fx0);
diff_normeX=eps2*(normeX+eps0) + 1;% pour rentrer au moins 1 fois dans la boucle
nb_eval_f = 0;
max_nb_eval_f = 1000;

s=0; %pas de cauchy
delta = delta0;
rho = 0;

while(((k < maxIter) && (diff_normeX > eps2*(normeX+eps0)) && (norm_gradfx > eps1*(norm(grad_fx0)+eps0)) && (nb_eval_f < max_nb_eval_f) ))
    %calcul du pas de cauchy
    grad_fx = grad_f(x);
    hess_fx = hess_f(x);
    s = cauchy(grad_fx, hess_fx, delta);

    %calcul du "rapport de satisfaction" de l'itération
    rho = (f(x) - f(x + s))/(-((grad_fx'*s) + 0.5*(s'*hess_fx*s)));
    nb_eval_f = nb_eval_f + 2;
    %% Mise à jour de l'itéré courant
    if (rho >= eta1)
        x = x + s;
        diff_normeX = norm(s);
        normeX = norm(x);
    else
        %x ne bouge pas
    end
    %% Mise à jour de la région de confiance
    if (rho >= eta2)
        delta = min(gamma2*delta, delta_max);
    elseif ((rho >= eta1) && (rho < eta2))
        %delta ne bouge pas
    else
        delta = gamma1*delta;
    end
    norm_gradfx = norm(grad_f(x));
    k = k+1;
end

x_min=x;
%% Positionnement du flag de sortie
if (k >= maxIter)
    flag = 1;
elseif (diff_normeX <= eps2*(normeX+eps0))
    flag = 2;
elseif (norm(grad_f(x)) <= eps1*(norm(grad_f(x0))+eps0))
    flag = 3;
elseif (nb_eval_f >= max_nb_eval_f)
    flag = 4;
else
    flag = -1;
end
%% Positionnement des sorties de l'algorithme
nb_iter = k;
f_xmin = f(x_min);
gradf_xmin = grad_f(x_min);

end

% rapport %
%interpretation mathematique rho : num, dénom > 0 (modèle décroit, fonction décroit), si rho = 1000 ca veut
%dire que le pas calculé fait décroitre la fonction très vite -> on est
%content

%f1 = modele quadratique de f1
%newton converge en 1 it. car prend la "bonne" direction de descente", RC
%descend dans la direction du gradient donc on change de direction souvent 
%donc meme si on prend des régions de confiances très grandes, on met 
%beaucoup d'itérations à converger vers la solution.
    