function [ f,g,H ] = f0()
%cette fonction sans paramètre renvoie la fonction f0, son gradient et sa
%jacobienne

f = @ff;
g = @gf;
H = @Hf;


function y = ff(x)
y= (x-1)*(x-2);
end

function y = gf(x)
y=2*x - 3;
end

function y = Hf(x)
y=2;
end
end

