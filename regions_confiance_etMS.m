function [x_min,flag,nb_eval_f,nb_iter,f_xmin, gradf_xmin]=regions_confiance_etMS(f, grad_f, hess_f,x0, eps1, eps2, delta_max, delta0, gamma1, gamma2, eta1, eta2)
%% Algorithme des régions de confiance avec la fonction etalonms
% Résout un problème de minimisation sans contraintes à l'aide de
% l'algorithme de Moré-Sorensen utilisé avec la fonction etalonms (fonction
% utilisée à des fins de test)
% Paramètres : 
% f, grad_f, hess_f : les fonctions f, gradient et hessienne de f
% x0 : point de départ de l'algorithme
% eps1 : tolérance sur la CN1 (norme de (gradient de f(x) )
% eps2 : tolérance sur la stagnation des itérés
% delta_max : largeur maximale des régions de confiance
% delta0 : largeur de la région de confiance initiale
% gamma1 : facteur de réduction de la région de confiance en cas d'échec
% de l'itération
% gamma2 : facteur d'augmentation de la région de confiance en cas de succès
% de l'itération
% eta1, eta2 : réel compris entre 0 et 1 ; si rho le rapport de
% satisfaction associé à l'itération est inférieur à eta1, alors
% l'itération est un échec ; si rho est compris entre eta1 et eta2 :
% l'itération est réussie : si rho est supérieur à eta2 : l'itération est
% très réussie
% Retour : 
% x_min : minimum de la fonction
% flag : indique la condition de sortie : 1 pour dépassement de maxIter, 2
% pour stagnation des itérés, 3 pour CN1 (norme du gradient du lagrangien),
% 4 pour dépassement de max_nb_eval_f
% nb_eval_f : nombre d'évaluations de la fonction f
% nb_iter : nombre d'itérations effectuées 
% f_xmin, gradf_xmin : valeurs de f et du gradient de f à la sortie de
% l'algorithme

maxIter = 100;
x=x0;
k=0;
normeX = norm(x);
grad_fx0 = grad_f(x0);
eps0 = 1; %garde fou pour conditions d'arret
norm_gradfx = eps1*(norm(grad_fx0)+eps0) + 1;
diff_normeX= eps2*(normeX+eps0)+1; % pour que cette condition ne soit pas fausse dès la première itération
nb_eval_f = 0;
max_nb_eval_f = 50000;

s=0;
delta = delta0;
rho = 0;

while(((k < maxIter) && (diff_normeX > eps2*(normeX+eps0)) && (norm_gradfx) > eps1*(norm(grad_fx0)+eps0)) && (nb_eval_f < max_nb_eval_f) ) ) %condition d'arret
    [ s, ~, lstar, ~ ] = etalonms( grad_f(x), hess_f(x), delta, 1e-8 );
    rho = (f(x) - f(x + s))/(-((grad_f(x)'*s) + 0.5*(s'*hess_f(x)*s)));

    nb_eval_f = nb_eval_f + 2;
    if (rho >= eta1)
        x = x + s;
        diff_normeX = norm(s);
        normeX = norm(x);
    else
        %x ne bouge pas
    end
    
    if (rho >= eta2)
        delta = min(gamma2*delta, delta_max);
    elseif ((rho >= eta1) && (rho < eta2))
        %delta ne bouge pas
    else
        delta = gamma1*delta;
    end
    k = k+1;
end

x_min=x;
if (k >= maxIter)
    flag = 1;
elseif (diff_normeX <= eps2*(normeX+eps0))
    flag = 2;
elseif (norm(grad_f(x)) <= eps1*(norm(grad_f(x0))+eps0))
    flag = 3;
elseif (nb_eval_f >= max_nb_eval_f)
    flag = 4;
else
    flag = -1;
end
nb_iter = k;
f_xmin = f(x_min);
gradf_xmin = grad_f(x_min);

end
