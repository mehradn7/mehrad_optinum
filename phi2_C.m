function [ phi,phip ] = phi2_C()
%cette fonction sans paramètre renvoie la fonction phi et sa dérivée

phi = @phiAUX;
phip = @phipAUX;

function [y] = phiAUX(x)
y = (4/(x-38)^2) + (400/(x+20)^2) - 0.7^2;
end

function [y] = phipAUX(x)
y = (-8/(x-38)^3) + (-800/(x+20)^3);
end


end

