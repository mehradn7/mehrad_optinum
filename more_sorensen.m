function [ s_min, lambda ] = more_sorensen( g, H, delta )
%% Algorithme des régions de confiance avec pas de Cauchy
% Calcule la solution du sous probleme des régions de
% confiance et lambda le multiplicateur de lagrange associe a la 
% contrainte norm(s) <= delta
% Paramètres : 
% g : Vecteur gradient de la fonction à minimiser
% H : Matrice hessienne de la fonction à minimiser
% delta : largeur de la région de confiance de ce sous-problème
% Retour : 
% s_min : solution du sous-problème sur la région de confiance
% lambda : multiplicateur de lagrange associé à la contrainte 

%% Calcul de la décomposition en valeurs propres de H
[Q,D] = eig(H);
eps = 1e-8; % tolérance sur la nullité des produits scalaires
vp_min = min(diag(D));


%% Recherche d'une solution intérieure
if ((vp_min > 0) && (norm(H\g) < delta))
    s_min = -H\g;
    lambda = 0;
    disp('sol intérieure vp_min>0');
elseif (vp_min == 0)
    D_et = diag(D);
    D_et = D_et(2:length(D_et));
    D_et = 1 ./ D_et;
    D_et = [0 ; D_et]; %D_et = diag(0,1/lambda2, ..., 1/lambdaN)
    s_barre = -Q*D_et*Q'*g;
    if (abs(Q(:,1)'*g) < eps) && (norm(s_barre) < delta) 
        s_min = s_barre;
        lambda = 0;
        disp('sol interieure');
        return;
    end
else
    %% pas de solution interieure, etape 2 : recherche sur la frontiere
    % Tri des vecteurs propres par ordre croissant des valeurs propres
    % associées
    [D, I] = sort(diag(D));
    Q = Q(:,I);
    % On récupère les handlers sur la fonction dont on cherche le zéro
    % ainsi que sa fonction dérivée
    [phi, phi_p] = S_tilde_delta2(Q,g,D, delta);
    % On récupère le handler sur la fonction nommée s_tilde (s dans le
    % sujet)
    [st] = S_tilde(Q,g,D);

    %% Déterminer l'intervalle de recherche de zéro de la fonction phi
    lambda_barre = max(0,-vp_min);
    %% Si le produit scalaire est non nul (norm(g) pour casser les facteurs
    % d'échelle)
    if (abs(Q(:,1)'*g) > eps*norm(g))
        lambda_min = lambda_barre + 1e-8; % on sait que lambda != lambda_min
        %donc on cherche la solution dans un intervalle ]lambda_min,
        %lambda_max]
        lambda_max = lambda_min + 10;
        while(phi(lambda_max)*phi(lambda_min) > 0)
            lambda_min = lambda_max;
            lambda_max = lambda_max + 10;
        end
        % On résout l'équation à l'aide de newton-dichotomie
        [lambda_sol,flag,nb_eval_phi,nb_iter,phi_lambdasol]=newtonNL(phi,phi_p,lambda_min,lambda_max,1e-6);
        % On calcule la solution et le lambda associé
        lambda = lambda_sol;
        s_min = st(lambda);
    else
        %% Sinon

        %% Si la norme de s(-lambda1) est supérieure à delta
       if ((norm(st(-vp_min)) >= delta))
           % idem, on détermine un intervalle sur lequel rechercher le zéro
           % de la fonction phi
           lambda_min = -vp_min;
           lambda_max = lambda_min + 10;
           while(phi(lambda_max) > 0)
               lambda_min = lambda_max;
               lambda_max = lambda_max + 10;
           end
           % On détermine lambda le zéro de la fonction et on en déduit
           % s_min
           [lambda_sol,flag,nb_eval_phi,nb_iter,phi_lambdasol]=newtonNL(phi,phi_p,lambda_min,lambda_max,1e-8);
            s_min = st(lambda_sol);
            lambda = lambda_sol;
       else
           %% Sinon : Hard case
           R = st(-vp_min);
           alpha = sqrt(delta^2 - norm(R)^2 );
           %On complète le vecteur R 
           s_min = R + alpha*Q(:,1);
           % s_min est alors de norme égale à delta
           lambda = -vp_min;
           disp('sol Hard Case');
       end
    end
end
end
