function [ s_min ] = cauchy( phi, jac_phi, delta )
%% Renvoie le pas de cauchy associe au sous-probleme de minimisation 
%quadratique
%   Paramètres : 
%   phi : le vecteur gradient de la quadratique
%   jac_phi : la matrice hessienne de la quadratique
%   delta : la région sur laquelle on minimise la quadratique
%   Retour : 
%   s_min : le minimum de la quadratique sur la boule de rayon delta

A = (phi')*jac_phi*phi; 
b = norm(phi);

%% Comme on se restreint à la direction du gradient, il s'agit simplement de minimiser un polynome réel de degré 2
if(b == 0)
    s_min = 0;
else
    if(A <= 0)
        t_min = delta/b;
    else
        t_min = min((b^2)/A,delta/b); 
    end
    s_min = -t_min*phi;
end


end

