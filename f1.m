function [ f,g,H ] = f1()
%cette fonction sans paramètre renvoie la fonction f1, son gradient et sa
%jacobienne

f = @ff;
g = @gf;
H = @Hf;

function [y] = ff(x)
y = 2*(x(1)+x(2)+x(3)-3)^2 + (x(1)-x(2))^2 + (x(2)-x(3))^2;
end

function [y] = gf(x)
y = zeros(3,1);
y(1) = 4*(x(1)+x(2)+x(3)-3) + 2*(x(1)-x(2));
y(2) = 4*(x(1)+x(2)+x(3)-3) - 2*(x(1)-x(2)) + 2*(x(2)-x(3));
y(3) = 4*(x(1)+x(2)+x(3)-3) - 2*(x(2)-x(3));
end

function [y] = Hf(x)
y = [6 2 4; 2 8 2; 4 2 6];
end

end

