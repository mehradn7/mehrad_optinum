function [ phi,phip ] = phi_C()
%cette fonction sans paramètre renvoie la fonction phi et sa dérivée

phi = @phiAUX;
phip = @phipAUX;

function [y] = phiAUX(x)
y = (4/(x+2)^2) + (36/(x+14)^2) - 0.5^2;
end

function [y] = phipAUX(x)
y = (-8/(x+2)^3) + (-72/(x+14)^3);
end


end

