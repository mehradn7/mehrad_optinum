function [ phi,phip ] = S_tilde_delta2(Q, g, D, delta)
%cette fonction sans paramètre renvoie la fonction s tilde et sa dérivée
%qui interviennent dans l'algorithme de moré sorensen

phi = @phiAUX;
phip = @phipAUX;

function [y] = phiAUX(lambda)
[st] = S_tilde(Q,g,D);
y = norm(st(lambda))^2 - delta^2;
end

function [y] = phipAUX(lambda)
C = Q'*g; %vecteur colonne
C = C .* C;
C = -2*C;
B = D + lambda;
B = (B .* B) .* B;
I = find(abs(C)>1e-8);
y = sum(C(I) ./ B(I));
end


end

