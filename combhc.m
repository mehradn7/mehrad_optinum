function [ comb_h_c ] = combhc(lambda, mu, c, sum_hess_ci)
%cette fonction renvoie un handler sur la fonction comb_h_c
% la fonction comb_h_c associe au couple (x,alpha) de même taille
%la somme des alpha_i*hess_c_i(x) (alpha réel, x vecteur)
% cette fonction est utile pour le calcul de la hessienne du lagrangien
%augmenté

comb_h_c = @comb_h_cAUX;


function [y] = comb_h_cAUX(x)
    y = (lambda + mu*c(x))' * (sum_hess_ci(x));
end


end