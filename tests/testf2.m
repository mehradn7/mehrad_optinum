addpath ../
%test avec f2
format shortE;

[f,g,H] = f2();

% x0 = [0;1/200 + 1/(10^12)];
% [x_min,flag,nb_eval_phi,nb_iter,phi_xmin] = newton(g,H,x0,1e-9, 1e-9)

% x0 = [-1.2;1];
% [x_min,flag,nb_eval_phi,nb_iter,phi_xmin] = newton(g,H,x0,1e-9, 1e-9)
% 
x0 = [10;0];
[x_min,flag,nb_eval_phi,nb_iter,phi_xmin] = newton(g,H,x0,1e-9, 1e-9)

%%tests f2 OK; pour le 3e point, la matrice est singuliere donc le systeme
%%lineaire (hessienne*dk = -gradient) n'admet pas de solution



