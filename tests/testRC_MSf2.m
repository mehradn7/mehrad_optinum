addpath ../
format shortE;


[f,g,H] = f2();

x0 = [0;1/200 + 1/(10^12)];
[x_min,flag,nb_eval_f,nb_iter,f_xmin, gradf_xmin]=regions_confiance_MS(f, g, H,x0, 1e-3, 1e-3, 10*norm(x0), 1, 0.5, 2, 0.25, 0.75);
x_min
nb_iter
gradf_xmin
flag
%OK, converge en 33 it

% x0 = [-1.2;1];
% [x_min,flag,nb_eval_f,nb_iter,f_xmin, gradf_xmin]=regions_confiance_MS(f, g, H,x0, 1e-3, 1e-3, 10*norm(x0), 1, 0.5, 2, 0.25, 0.75);
% x_min
% nb_iter
% gradf_xmin
% flag
% OK (34 it)

% x0 = [10;0];
% [x_min,flag,nb_eval_f,nb_iter,f_xmin, gradf_xmin]=regions_confiance_MS(f, g, H,x0, 1e-6, 1e-6, 10*norm(x0), 1, 0.5, 2, 0.25, 0.75);
% x_min
% nb_iter
% gradf_xmin
% flag
%OK (46 it)


