addpath ../
g1 = [0;0];
H1 = [7 0;0 2];

s_min = cauchy(g1, H1, 1)

g2 = [6;2];
H2 = H1;

%delta_k = 0.97 environ
s_min = cauchy(g2, H2, 0.98)

g3 = [-2;1];
H3 = [-2 0;0 10];

%si on change dans H3 le 10 par 5 on passe de convexe(a>0) à concave(a<0)
s_min = cauchy(g3, H3, 1)

%pour les tests : calculer le delta_k de "basculement" tel que delta_k =
%t_lim(=-b/a)