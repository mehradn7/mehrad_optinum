addpath ../
format shortE;

%%Tests sur f1
% [f,g,H] = f1();
% [contraintef1, jac_cf1, somme_hess_cf1] = cf1();
% xc11 = [0;1;1];
% xc12 = [0.5;1.25;1];
% 
% [x_min,lambda,mu,flag,nb_eval_f,nb_iter,f_xmin, gradf_xmin]=lagrangien_augmente(f, g, H, contraintef1, jac_cf1, somme_hess_cf1,  xc11 , 0, 2, 0.1, 0.9, 1e-6, 1e-6, 3);
% %avec RC Cauchy, pour eps3=1e-6 on ne converge jamais (la norme du
% %lagrangien ne passe jamais en dessous de 1e-6)
% x_min
% gradf_xmin
% flag
% nb_iter
% Tout converge (sauf avec RC Cauchy pour certains eps3)

% %Tests sur f2
[f,g,H] = f2();
[contraintef2, jac_cf2, somme_hess_cf2] = cf2();
xc21 = [0.5*sqrt(3);0.5*sqrt(3)];
xc22 = [0.5*sqrt(3);0.5*sqrt(3)];
[x_min,lambda,mu,flag,nb_eval_f,nb_iter,f_xmin, gradf_xmin]=lagrangien_augmente(f, g, H, contraintef2, jac_cf2, somme_hess_cf2,  xc22, 1.3, 2, 0.1, 0.9, 1e-6, 1e-8, 2);

x_min
gradf_xmin
flag
nb_iter
%Tout converge