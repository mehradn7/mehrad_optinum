addpath ../
g1 = [0;0];
H1 = [7 0;0 2];

s_min = cauchy(g1, H1, 1);
[sms,dms,lstar,flag] = etalonms(g1,H1,1,1e-8)
[s_min2, lambda] = more_sorensen(g1, H1, 1)


g2 = [6;2];
H2 = H1;

%delta_k = 0.97 environ
%TODO programme de test pour vérifier ça
s_min = cauchy(g2, H2, 0.98);
[sms,dms,lstar,flag] = etalonms(g2,H2,0.98,1e-8)
[s_min2, lambda] = more_sorensen(g2, H2, 0.98)

g3 = [-2;1];
H3 = [-2 0;0 10];

s_min = cauchy(g3, H3, 1);
[sms,dms,lstar,~] = etalonms(g3,H3,2,1e-8)
[s_min2, lambda] = more_sorensen(g3, H3, 2)

% LES 3 MARCHENT

g4 = [0;0];
H4 = [-2 0;0 10];

s_min = cauchy(g4, H4, 1);
[sms,dms,lstar,~] = etalonms(g4,H4,2,1e-8)
[s_min2, lambda] = more_sorensen(g4, H4, 2)

g5 = [2;3];
H5 = [4 6;6 5];

s_min = cauchy(g5, H5, 1);
[sms,dms,lstar,~] = etalonms(g5,H5,2,1e-8)
[s_min2, lambda] = more_sorensen(g5, H5, 2)

g6 = [2;0];
H6 = [4 0;0 -15];

s_min = cauchy(g6, H6, 2);
[sms,dms,lstar,~] = etalonms(g6,H6,2,1e-8)
[s_min2, lambda] = more_sorensen(g6, H6, 2)

%meme test mais avec delta=0.01 pour rentrer dans le cas norm(s)>delta
% s_min = cauchy(g6, H6, 2);
% [sms,dms,lstar,~] = etalonms(g6,H6,0.01,1e-8)
% [s_min2, lambda] = more_sorensen(g6, H6, 0.01)

%LES 6 MARCHENT