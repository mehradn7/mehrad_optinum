function [lambda_sol,flag,nb_eval_phi,nb_iter,phi_lambdasol] = newtonNL( phi, phi_p, lambda_min, lambda_max, eps) 
%% Algorithme de Newton non linéaire (ou Newton dichotomie)
% Méthode de Newton pour une fonction non linéaire de la variable réelle
% pour une fonction phi donnée, renvoie une approximation de la solution
% de l'équation phi(lambda) = 0
% Paramètres : 
% phi, phi_p : la fonction dont on cherche le zéro et sa dérivée
% lambda_min, lambda_max : bornes de l'intervalle de recherche de zéro
% eps : tolérance sur le zéro
% Retour : 
% lambda_sol : zéro de la fonction phi
% flag : indique la condition de sortie : 1 pour dépassement de maxIter, 2
% pour annulation de la fonction, 42 si l'une des bornes est un zéro de la
% fonction
% nb_eval_phi : nombre d'évaluations de la fonction phi
% nb_iter : nombre d'itérations effectuées
% phi_lambdasol : valeur de la fonction en le zéro calculé

%% Initialisation des constantes et variables
maxIter = 50;
k = 0;
nb_eval_phi = 0;

phi_lambdamin = phi(lambda_min);
phi_lambdamax = phi(lambda_max);

%% On vérifie si l'une des bornes de l'intervalle n'est pas zéro de la fonction
min_frontieres = min(abs(phi_lambdamin), abs(phi_lambdamax));
if (abs(min_frontieres) < eps)
    if ((abs(phi_lambdamin)) == min_frontieres)
        lambda_sol = lambda_min;
        phi_lambdasol = phi_lambdamin;
    else
        lambda_sol = lambda_max;
        phi_lambdasol = phi_lambdamax;
    end

    flag = 42;%la solution se trouve sur les frontières initiales
else
    lambda = lambda_max;
    phi_lambda = phi(lambda);
    nb_eval_phi = 3;
    
    %% Sinon, on procède par dichotomie 
    while( (k < maxIter) && (norm(phi_lambda) > eps ) )% on a retiré le cassage de facteur d'échelle car 
        % il nous faisait diverger dans certains cas 
        % calcul de l'itéré
        lambda_N = lambda - (phi_lambda/phi_p(lambda));
        nb_eval_phi = nb_eval_phi + 2;
        
        %si l'itéré est accepté
        if( (lambda_N >= lambda_min) && (lambda_N <= lambda_max) && ( abs(phi(lambda_N)) < 0.5*abs(phi_lambda)) )
            lambda = lambda_N;
            nb_eval_phi = nb_eval_phi + 1;
        else
            %sinon, réduire l'intervalle de recherche de moitié
            lambda_D = (lambda_min + lambda_max)/2;
            if (phi(lambda_D)*phi(lambda_max) <= 0)
                lambda_min = lambda_D;
            else
                lambda_max = lambda_D;
            end
            nb_eval_phi = nb_eval_phi + 2;
            lambda = lambda_D;
        end
        phi_lambda = phi(lambda);
        k = k+1;
    end
    
    %% Positionnement des flags de sortie et des sorties de l'algorithme
    lambda_sol = lambda;
    phi_lambdasol = phi(lambda);
    
    if (k >= maxIter)
        flag = 1;
    elseif (norm(phi(lambda)) <= eps)
        flag = 2;
    else
        flag = -1;
    end
    nb_eval_phi = nb_eval_phi + 2;
    
end

nb_iter = k;

end

